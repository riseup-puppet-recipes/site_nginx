class site_nginx inherits nginx::base {
  Exec ['force-reload-nginx'] {
    subscribe => [ File["/crypt/certs/wildcard/key.pem"], File["/crypt/certs/wildcard/cert.pem"], File["/crypt/certs/wildcard/combined.pem"] ],
  }

  include nginx::debian
}
